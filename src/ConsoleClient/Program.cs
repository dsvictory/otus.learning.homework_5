﻿using IdentityModel.Client;// необходимо для взаимодействия с IS4
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleClient {
    class Program {
        static async Task Main(string[] args) {
            var httpClientHandler = new HttpClientHandler();
            // allow untrusted SSL certificates
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            // получаем метаданные openId нашего IS4
            var client = new HttpClient(httpClientHandler);

            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError) {
                Console.WriteLine(disco.Error);
                return;
            }

            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest() {
                Address = disco.TokenEndpoint,
                ClientId = "m2m.client",
                ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A",
                Scope = "scope1",

                UserName = "bob",
                Password = "bob"
            });

            if (tokenResponse.IsError) {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");

            // call api
            var apiClient = new HttpClient(httpClientHandler);
            apiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await apiClient.GetAsync("https://localhost:44326/identity");
            if (!response.IsSuccessStatusCode) {
                Console.WriteLine(response.StatusCode);
            }
            else {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
            }

            Console.WriteLine("________________________________________________________________________");
            var helloResponse = await apiClient.GetAsync("https://localhost:44326/Home");
            if (!helloResponse.IsSuccessStatusCode) {
                Console.WriteLine(helloResponse.StatusCode);
            }
            else {
                var content = await helloResponse.Content.ReadAsStringAsync();
                Console.WriteLine(content);
            }
            Console.ReadLine();
        }
    }
}
