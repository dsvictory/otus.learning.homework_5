﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceApi.Controllers {

    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class HomeController : ControllerBase {

        [HttpGet]
        public string Get() {
            return "Hello, world!";
        }
    }
}
